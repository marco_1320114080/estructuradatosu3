class Arbol
{
    constructor()
    {
        this.nodoPadre = this.agregarNodoPadre();
        //this.nivel = 2;
        //this.busquedaElemento = '';
        //this.buscarNodo = [];
        //this.caminoNodo = '';
        //this.sumarCamino = 0;

    }

    agregarNodoPadre()
    {
        var nodo= new Nodo (null, null, "+", null);
        return nodo;

    }

    agregarNodo(nodoPadre, posicion, nombre, valor)
    {
        var nodo = new Nodo (nodoPadre, posicion, nombre, valor);
        return nodo;

    }

    verificarNivelHijos(nodo)
    {
        if(nodo.nivel==this.nivel)
            this.buscarNodo.push(nodo.nombre);
        if(nodo.hasOwnProperty('hI'))
            this.verificarNivelHijos(nodo.hI);
        if(nodo.hasOwnProperty('hD'))
            this.verificarNivelHijos(nodo.hD);

        return this.buscarNodo;
    }

    buscarValor(buscarElemento, nodo=Nodo = this.nodoPadre)
    {
        if(nodo.valor == buscarElemento)
            this.busquedaElemento = nodo;
        if (nodo.hasOwnProperty('hI'))
            this.buscarValor(buscarElemento, nodo.hI );
        if (nodo.hasOwnProperty('hD'))
            this.buscarValor(buscarElemento, nodo.hD );

        return this.busquedaElemento;
    }

    buscarCaminoNodo(nodo)
    {
        if(nodo.padre != null)
        {
            this.caminoNodo=this.caminoNodo+' '+nodo.padre.nombre;
            this.buscarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento+ ' ' + this.caminoNodo;
    }

    sumarCaminoNodo(nodo)
    {
        if(nodo.padre != null) {
            this.sumaDeCaminos = this.sumaDeCaminos + nodo.padre.valor;
            this.sumarCaminoNodo(nodo.padre);
        }
        return this.busquedaElemento.valor+this.sumarCamino;
    }
}